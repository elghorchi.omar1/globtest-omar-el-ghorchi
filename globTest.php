<?php

############## The function foo() ###########################

function foo($tabs)
{
    usort($tabs, function ($tab1, $tab2) {
        return $tab1[0] - $tab2[0];
    });

    $unioned = [];
    $unioned[] = $tabs[0];

    for ($i = 1; $i < count($tabs); $i++) {
        $current = &$unioned[count($unioned) - 1];
        if ($current[1] >= $tabs[$i][0]) {
            $current[1] = max($current[1], $tabs[$i][1]);
        } else {
            $unioned[] = $tabs[$i];
        }
    }

    return $unioned;
}

############## la fonction de l'affichage ###########################

function toString($key, $result)
{
    echo "case $key ==> ";
    foreach ($result as $interval) {
        echo "[" . $interval[0] . ", " . $interval[1] . "] ";
    }
    echo PHP_EOL;
}

################ vérification du format du cas de test  ###################
function assertFormat($tests)
{
    if (!is_array($tests) || empty($tests)) {
        return false;
    }

    foreach ($tests as $test) {
        if (
            !is_array($test) ||
            count($test) !== 2 ||
            !is_numeric($test[0]) ||
            !is_numeric($test[1]) ||
            $test[1] <= $test[0]
        ) {
            return false;
        }
    }

    return true;
}

############## les cas de tests ###########################

$testTabs = [
    [], // empty table
    [[3, 0], [5, 10]], // [3,0] Not a valid interval
    "invalid", // string
    [[0, 3], [6, 10]],
    [[0, 5], [3, 10]],
    [[0, 5], [2, 4]],
    [[7, 8], [3, 6], [2, 4]],
    [[3, 6], [3, 4], [15, 20], [16, 17], [1, 4], [6, 10], [3, 6]],
];

################ Exécution des tests ###################
foreach ($testTabs as $key => $testTab) {
    if (true != assertFormat($testTab)) {
        echo "Test case number $key is in an invalid format." . PHP_EOL;
        continue;
    }

    $union = foo($testTab);
    toString($key, $union);
}
?>
