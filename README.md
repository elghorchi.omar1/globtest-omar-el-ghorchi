# GlobTest Omar EL GHORCHI

## Guide d'utilisation :

- Clonez le projet ou téléchargez le fichier globTest.php.
- Accédez au fichier par le terminal.
- Lancez la commande suivante :

```bash
 php globTest.php
 ```

## Question 1:

D'après le comportement de la fonction foo(), il semble qu'elle prenne en entrée une liste de plages représentée sous forme d'un tableau de deux éléments, et qu'elle fusionne les plages qui se chevauchent ou se touchent. Ensuite, elle renvoie une liste de plages qui ne se chevauchent ni ne s'intersectent.


## Question 2:

Vous trouverez la solution dans le fichier globTest.php, incluant :

- L'implémentation de la fonction foo()
- La fonction d'affichage toString()
- La vérification du format du cas de test assertFormat()
- Les cas de tests
- L'exécution des tests

## Question 3:

Le temps d'implémentation est réparti comme suit :

- Investigation : 20 minutes
- Développement : 1 heure 30 minutes
- Écriture de code propre, documentation README et gestion Git : 40 minutes